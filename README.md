# PSI-02

## Sistem-Informasi-Komoditas-Dinas-Pertanian-SIKDP
===============================================================================

A Spring Boot Project {Thymeleaf, JPA, MySql | HSQL, Spring MVC, Spring Security}

Main features:

* Authentication (registration, login)
* Basic CRUD, File Uploading, Selection, Payment, and Announcement

===============================================================================

DEMO : [On You Tube](https://www.youtube.com/watch?v=N3SbeZRr2dY&feature=youtu.be)

Use database: "db_pendaftaran.sql" that already attached to the root of the directory. 

Current user:

++ADMIN++

* Email : panitia@gmail.com
* Pass : panitia11

++KETUA++

* Email : user@gmail.com
* Pass : user11

SOME SCREENSHOTS:

![Poster PSI-02](https://github.com/arimnrg16/PSI-02/blob/master/Poster%20PSI-02/1.png)
![Poster PSI-02](https://github.com/arimnrg16/PSI-02/blob/master/Poster%20PSI-02/2.png)

![Screenshoot PSI-02](https://github.com/arimnrg16/PSI-02/blob/master/Screenshots/Screenshot%20(97).png)
![Screenshoot PSI-02](https://github.com/arimnrg16/PSI-02/blob/master/Screenshots/Screenshot%20(98).png)
![Screenshoot PSI-02](https://github.com/arimnrg16/PSI-02/blob/master/Screenshots/Screenshot%20(99).png)
![Screenshoot PSI-02](https://github.com/arimnrg16/PSI-02/blob/master/Screenshots/Screenshot%20(100).png)
![Screenshoot PSI-02](https://github.com/arimnrg16/PSI-02/blob/master/Screenshots/Screenshot%20(101).png)
![Screenshoot PSI-02](https://github.com/arimnrg16/PSI-02/blob/master/Screenshots/Screenshot%20(102).png)
